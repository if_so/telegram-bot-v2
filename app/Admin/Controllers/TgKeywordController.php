<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\TgKeyword;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Controllers\AdminController;
use DB;

class TgKeywordController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new TgKeyword(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('bot_id')->display(function (){
                $bot = DB::table("tg_bot")->where("id",$this->bot_id)->first();
                return '<span class="label bg-primary">'.$bot->username."[".$bot->id."]".'</span>';
            });
            $grid->column('keyword');
            // $grid->column('type');
            $grid->column('content')->width('270px');
            $grid->column('status')->switch();
            $grid->column('remark');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
            
            $grid->disableQuickEditButton();
            $grid->quickSearch();
            $grid->quickSearch('keyword', 'content','id');
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
                $bots = DB::table("tg_bot")->get()->pluck('username', 'id');
                $filter->in('bot_id')->multipleSelect($bots);
                $filter->like('keyword');
                $filter->like('content');
                $filter->between('created_at', "created_at")->datetime();
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new TgKeyword(), function (Show $show) {
            $show->field('id');
            $show->field('bot_id');
            $show->field('keyword');
            // $show->field('type');
            $show->field('content');
            $show->field('status');
            $show->field('remark');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new TgKeyword(), function (Form $form) {
            $form->display('id');
            $form->text('bot_id');
            $form->text('keyword');
            // $form->text('type');
            $form->markdown('content');
            $form->text('status');
            $form->text('remark');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
