<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    
    $router->post('setLang', 'NavController@setLang');
    
    $router->resource('tgBot', 'TgBotController'); // 机器人列表
    $router->resource('tgBot_msg_record', 'TgMsgRecordController'); //消息记录
    $router->resource('tg_keyword', 'TgKeywordController'); //回复设置
    $router->resource('tg_group', 'TgGroupController'); //群列表

});
